# Four-bar Linkage Example

1. [On Windows](#on-windows)
   1. [Download scene](#download-scene)
   2. [Open scene](#open-scene)
2. [On Linux (WSL)](#on-linux-wsl)
   1. [Git clone](#git-clone)
   2. [Start the project](#start-the-project)

This example uses [ROS](http://www.ros.org/) installed on WSL and [V-REP](http://www.coppeliarobotics.com/) installed on Windows. You can use V-REP of WSL, but you will experience lower performance than Windows.
The overall structure is as follows.

![structure](img/structure.png)

## On Windows

> If you are using native linux, follow these steps in linux. The usage of the scene is the same.

### Download scene

Download this file to your preferred location.
[Download link](https://gitlab.com/rise-lab/four_bar_example/blob/master/vrep_scenes/4_bar_example.ttt)

![scene_download](img/scene_download.PNG)

### Open scene

The extension of vrep scene file is `*.ttt`. You can open these files by double clicking them, or `[File] > [Open scene ...]` in vrep.

![vrep scene](img/open_scene.PNG)

## On Linux (WSL)

### Git clone

[Git](https://en.wikipedia.org/wiki/Git) is one of the most useful tools.
You can download the repositories stored online by using the `git clone` command.
This page you are reading now is also a repository.

1. Clone this repository in your ros workspace

    ```bash
    cd ~/catkin_ws/src/
    git clone https://gitlab.com/rise-lab/four_bar_example.git
    ```

### Start the project

1. Use `chmod` to change the access permissions of the file. The name is an abbreviation of [change mode](https://en.wikipedia.org/wiki/Chmod). Otherwise, the ROS will not be able to execute the Python file.

    ```bash
    chmod 777 ~/catkin_ws/src/four_bar_example/script/main.py
    ```

2. Enter the following command and then your vrep will work.

    ```bash
    roslaunch four_bar_example run.launch
    ```
    ![hello ros, hello vrep](img/working.PNG)
