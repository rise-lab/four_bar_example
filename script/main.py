#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: Hong-ryul Jung (RISE)

import numpy as np
import rospy
from vrepsim import VrepSimulation


class Main:
    objnames = ['joint1']
    
    def __init__(self):
        self.vrep = VrepSimulation(self.objnames)

    def main(self):
        if self.vrep.is_not_ready:
            raise NotImplementedError
        else:
            self.vrep.start_simulation()

        vel = np.deg2rad(90) # rad/sec
        quarter = np.deg2rad(45)
        target = quarter

        rate = rospy.Rate(30)
        while not rospy.is_shutdown():
            rotation = self.vrep.get_joint_position('joint1')            
            info = "The rotation angle is "+str(round(rotation, 3))+" rad."
            rospy.loginfo(info)
            
            if ((target > 0) and (rotation > target)) or\
                ((target < 0) and (rotation < target)):
                vel *= -1.0
                target *= -1.0

            self.vrep.set_joint_target_velocity('joint1', vel)
            rate.sleep()


if __name__ == "__main__":
    rospy.init_node('main')
    try:
        m = Main()
        m.main()
    except rospy.ROSInterruptException:
        pass
